'use strict';

const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());

let contacts = require('./data2');

app.get('/todo', (request, response) => {
  if (!contacts) {
    response.status(404).json({ message: 'No contacts found.' });
  }
  response.json(contacts);
});

app.get('/todo/:id', (request, response) => {

  let contactId = request.params.id;

  let contact = contacts.filter(contact => {
    return contact.id == contactId;
  });

  if (!contact) {
    response.status(404).json({ message: 'Contact not found' });
  }

  response.json(contact[0]);
});

app.post('/todo', (request, response) => {
  let obj = contacts.find(obj => obj.name == request.body.name);
  if (!obj) {
    let contact = {
    
      id: contacts.length + 1,
      name: request.body.name,
      value: request.body.value,
      balance : request.body.value
    };
    contacts.push(contact)
    response.json(contact);
  } else {
    var obj2 = obj.name;
    if(obj2 === request.body.name){
  
    var name2 =  contacts.filter(function(nm) {
      return nm.name === request.body.name });
  
    var value2 =  name2.map(function(val) {
      return val.value });
      const reducer = (accumulator, currentValue) => accumulator + currentValue;
       const sum = value2.reduce(reducer);
       const sum2 = sum + request.body.value;
       let contact = {
    
        id: contacts.length + 1,
        name: request.body.name,
        value: request.body.value,
        balance : sum2
      };
      contacts.push(contact);
      response.json(contact);
    }
     
  
 
  contacts.push(contact);
  response.push(contact);
}


  
});

app.put('/todo/:id', (request, response) => {

  let contactId = request.params.id;

  let contact = contacts.filter(contact => {
    return contact.id == contactId;
  })[0];

  const index = contacts.indexOf(contact);

  let keys = Object.keys(request.body);

  keys.forEach(key => {
    contact[key] = request.body[key];
  });

  contacts[index] = contact;

  // response.json({ message: `User ${contactId} updated.`});
  response.json(contacts[index]);
});

app.delete('/todo/:id', (request, response) => {
  
  let contactId = request.params.id;

  let contact = contacts.filter(contact => {
    return contact.id == contactId;
  })[0];

  const index = contacts.indexOf(contact);

  contacts.splice(index, 1);

  response.json({ message: `User ${contactId} deleted.`});

});

const hostname = 'localhost';
const port = 3000;

const server = app.listen(port, hostname, () => {

  console.log(`Server running at http://${hostname}:${port}/`);
  
});
